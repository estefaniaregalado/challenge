import React, { Component } from 'react';
import Carousel from '../components/Carousel';
import { withRouter } from 'react-router-dom';
import { Link } from "react-router-dom";
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';

const ArrowLink = styled(Link)`
   color: ${props => props.theme.black};
`
const DetailCard = styled.div`
    margin-bottom: 20px;

    img {
        width: 100%;
        height: auto;
    }
`

class Detail extends Component {
    componentDidMount() {
        // Avoid calling fetchPopularCards if popularCards already exists
        if(this.props.popularCards.length === 0) {
            this.props.fetchPopularCards();
        }
        this.props.fetchSelectedCard(this.props.match.params.id);
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.match.params.id !== prevProps.match.params.id) {
            this.props.fetchSelectedCard(this.props.match.params.id);
        }
    }

    componentWillUnmount() {
        this.props.clearSelectedCard();
    }

    render() {
        const {popularCards, card} = this.props;
        return(
            <>
                {
                    (card && card.image) && 
                    <DetailCard>
                        <h3>
                            <ArrowLink to={`/`}><FontAwesomeIcon icon={faChevronLeft}/></ArrowLink> {card.title ? card.title : 'A gif'}
                        </h3>
                        <img src={card.image.url} alt={card.title}/>
                    </DetailCard>
                }
                <Carousel list={popularCards}/> 
            </>
        )
       
    }
}

export default withRouter(Detail);