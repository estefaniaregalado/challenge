import React, { Component } from 'react';
import SearchBarContainer from '../containers/SearchBarContainer';
import ListContainer from '../containers/ListContainer';

class Home extends Component {
    componentDidMount() {
        this.props.fetchCards();
    }

    render() {
        return(
            <>
                <SearchBarContainer/>
                <ListContainer />
            </>
        )
       
    }
}


export default Home;