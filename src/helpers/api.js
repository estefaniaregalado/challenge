import { ENDPOINT } from '../constants';
import { API_KEY } from '../config';
import { compose } from 'rambda';

const getParams  = (params = {}) => Object.entries(params).map(([key, val]) => `${key}=${val}`).join('&');
const doFetch = (url) => 
    fetch(url)
        .then(response => response.json())
        .then(response => response.data)
        .catch(error => ({ error }));

const getCardsUrl = (params) => `${ENDPOINT}/gifs/search?api_key=${API_KEY}&${params}`;
const getPopularCardsUrl = (params) => `${ENDPOINT}/gifs/trending?api_key=${API_KEY}&${params}`;
const getCardByIdUrl = id => `${ENDPOINT}/gifs/${id}?api_key=${API_KEY}`;

export const getCards = compose(doFetch, getCardsUrl, getParams);
export const getPopularCards = compose(doFetch, getPopularCardsUrl, getParams);
export const getCardById= compose(doFetch, getCardByIdUrl);
