import {
    FETCH_CARDS,
    UPDATE_ALL_CARDS,
    FETCH_POPULAR_CARDS,
    UPDATE_POPULAR_CARDS,
    FETCH_SELECTED_CARD,
    UPDATE_SELECTED_CARD,
    CLEAR_SELECTED_CARD
} from '../constants';
  
  
export const fetchCards = query => ({
    type: FETCH_CARDS,
    payload: {
        query
    }
});

export const fetchPopularCards = query => ({
    type: FETCH_POPULAR_CARDS,
    payload: {
        query
    }
});

export const updateAllCards = cards => ({
    type: UPDATE_ALL_CARDS,
    payload: {
        cards
    }
});

export const updatePopularCards = cards => ({
    type: UPDATE_POPULAR_CARDS,
    payload: {
        cards
    }
});

export const fetchSelectedCard = id => ({
    type: FETCH_SELECTED_CARD,
    payload: {
        id
    }
});

export const updateSelectedCard = card => ({
    type: UPDATE_SELECTED_CARD,
    payload: {
        card
    }
});

export const clearSelectedCard = card => ({
    type: CLEAR_SELECTED_CARD
});
  

