import {
    ERROR,
    LOADING
} from '../constants';
   
export const setError = error => ({
    type: ERROR,
    payload: {...error}
});

export const setLoading = () => ({
    type: LOADING
});