import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';
import styled, { ThemeProvider } from 'styled-components'
import HomeContainer from './containers/HomeContainer';
import DetailContainer from './containers/DetailContainer';
import { GlobalStyle, theme } from './theme/globalStyle';

const AppHeader = styled.header`
    background: ${props => `linear-gradient(to bottom, ${props.theme.purple} 0%, ${props.theme.darkPurple} 100%)`};
    text-align: center;
    color: white;
    font-size: 24px;
    padding: 30px 0;
`;

const AppTitle = styled.h1`
  font-weight: 500;
  margin: 0;
  padding: 0;

  strong {
    color: #8ee800;
    }
`

const Container = styled.section`
    margin: 0 auto;
    max-width: 1004px;
    padding: 0 15px;
`;


const App = () =>
    (
        <Router>
            <ThemeProvider theme={theme}>
                <GlobalStyle />
                <main className="App">
                    <AppHeader>
                        <Container>
                            <AppTitle>Pop<strong>Gifs</strong>!</AppTitle>
                        </Container>
                    </AppHeader>
                    <Container>
                        <Switch>
                            <Route exact path="/" component={HomeContainer}/>
                            <Route path="/gif/:id" component={DetailContainer}/>
                        </Switch>
                    </Container>
                </main>
            </ThemeProvider>
        </Router>
    );

export default App;
