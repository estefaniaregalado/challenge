import styled,  { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
    body {
        padding: 0;
        margin: 0;
        font-family: sans-serif;
    }

    .slick-next:before, 
    .slick-prev:before {
        color: ${props => props.theme.black};
        font-family: "Font Awesome 5 Free"; 
        font-weight: 900; 
        content: "\f053";
        font-size: 25px;

        &:hover {
            color: ${props => props.theme.darkPurple};
        }
    }
    

    .slick-prev:before {
        content: "\f053";
    }

    .slick-next:before {
        content: "\f054";
    }

`
export const theme = {
    purple: '#9013d3',
    darkPurple: '#560d87',
    gray: '#d2d2d2',
    white: '#ffffff',
    black: '#000000'
}

export const Button = styled.button`
    font-size: 14px;
    padding: 14px 20px;
    margin: 0;
    background: transparent;
    color: white;
    background: ${props => props.theme.darkPurple};
    border: 0;
    border-radius: 0;
    cursor: pointer;
    font-weight: bold;
`

export const InputText = styled.input`
    border: 2px solid ${props => props.theme.darkPurple};
    border-radius: 0;
    padding: 12px 10px;
    font-size: 16px;
    background: ${props => props.theme.gray};
`;