import React from 'react';
import Card from './Card';
import styled from 'styled-components';
import Slider from "react-slick";

const Title = styled.h3`
    margin: 0 0 15px; 
`;



const settings = {
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
    
};

const Carousel = ({ list = [] }) => (
    <>
        <Title>See other popular images</Title>
        <Slider {...settings}>
            {list.map(card => (
                <Card key={card.id} card={card} />
            ))}
        </Slider>
    </>
    
  );

export default Carousel;