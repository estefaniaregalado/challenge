import React, { useState } from 'react';
import { Button, InputText } from '../theme/globalStyle';
import styled from 'styled-components';

const SearchBarWrapper = styled.div`
    display: flex;  
    margin-bottom: 20px; 
`

const SearchBarButton = styled(Button)`
    width: 25%;
`

const SearchBarInput = styled(InputText)`
    width: calc(75% - 5px);
    margin-right: 5px;
`

const SearchBar = ({ fetchCards }) => {
    const [query, setQuery] = useState('');

    const handleChange = (event) => setQuery(event.target.value);

    return (
        <>
            <h3>Search your Interests</h3>

            <SearchBarWrapper>
                <SearchBarInput type="text" value={query} onChange={e => handleChange(e)}/>
                <SearchBarButton onClick={() => fetchCards(query)}>Search</SearchBarButton>
            </SearchBarWrapper>
        </>
    )
};

export default SearchBar;