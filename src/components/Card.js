import React from 'react';
import styled from 'styled-components';
import { Link } from "react-router-dom";

const CardItem = styled.div`
    width: 100%;
    margin: 15px 0;
    position: relative;
    flex-shrink: 0;
    overflow: hidden;
    height: 200px;
    background-image: url(${props => props.img});
    background-size: cover;
    background-position: center;
    border: 2px solid transparent;

    @media only screen and (min-width: 768px) {
        width: calc(33.33% - 18px);
        margin: 15px 7px;
    }

    .slick-slide & {
        width: calc(100% - 20px);
        margin: 0 10px;
    }

    &:hover {
        border-color: ${props => props.theme.darkPurple};
    }

    img {
        display: none;
    }

    h3 {
        position: absolute;
        margin: 0;
        bottom: 20px;
        left: 20px;
        right: 20px;
        color: ${props => props.theme.white};
    }
    
`;

const CardLink = styled(Link)`
   display: block;
   width: 100%;
   height: 100%;
`

const Card = ({card}) => (
    <CardItem img={card.thumb.url}>
        <CardLink to={`/gif/${card.id}`}>
            <img src={card.thumb.url} alt=""/>
            <h3>{card.title}</h3>
        </CardLink>
    </CardItem>
  );

export default Card;