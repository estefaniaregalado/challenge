import React from 'react';
import Card from './Card';
import styled from 'styled-components'

const CardsList = styled.div`
    padding: 0;
    display: flex;
    flex-wrap: wrap;

    @media only screen and (min-width: 768px) {
        margin: 0 -7px;
    }
`;

const List = ({ list = [] }) => (
    <CardsList>
        {list.map(card => (
            <Card key={card.id} card={card} />
        ))}
    </CardsList>
  );

export default List;