import { connect } from 'react-redux';
import List from '../components/List';

const mapStateToProps = state => {
    return { list: state.cards };
};

export default connect(
    mapStateToProps
)(List);
