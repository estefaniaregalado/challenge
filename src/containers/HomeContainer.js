import { connect } from 'react-redux';
import Home from '../pages/Home';
import { fetchCards } from '../actions/cardsActions'

const mapDispatchToProps = {
    fetchCards
};

export default connect(
    null,
    mapDispatchToProps
)(Home);
