import { connect } from 'react-redux';
import Detail from '../pages/Detail';
import { fetchPopularCards, fetchSelectedCard, clearSelectedCard } from '../actions/cardsActions'

const mapStateToProps = (state) => {
    return { popularCards: state.popularCards, card: state.selectedCard };
};

const mapDispatchToProps = {
    fetchPopularCards,
    fetchSelectedCard,
    clearSelectedCard
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Detail);
