import { connect } from 'react-redux';
import SearchBar from '../components/SearchBar';
import { fetchCards } from '../actions/cardsActions'

const mapDispatchToProps = {
    fetchCards
};

export default connect(
    null,
    mapDispatchToProps
)(SearchBar);
