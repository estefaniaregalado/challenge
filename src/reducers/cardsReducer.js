import { UPDATE_ALL_CARDS } from '../constants';
import { cardsDataNormalize } from './_utilities';


const cardsReducer = (cards = [], action) => {
  if (action.type === UPDATE_ALL_CARDS) {
    return cardsDataNormalize(action.payload.cards)
  }

  return cards;
}

export default cardsReducer;