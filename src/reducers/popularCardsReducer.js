import { UPDATE_POPULAR_CARDS } from '../constants';
import { cardsDataNormalize } from './_utilities';

const popularCardsReducer = (cards = [], action) => {
  if (action.type === UPDATE_POPULAR_CARDS) {
    return cardsDataNormalize(action.payload.cards)
  }

  return cards;
}

export default popularCardsReducer;