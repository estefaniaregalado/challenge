import { UPDATE_POPULAR_CARDS } from '../constants';
import popularCardsReducer from './popularCardsReducer';


const initalState = [];

describe('test popularCards reducer', () => {
    it('updates all items in the array', () => {
        expect(popularCardsReducer(initalState, { 
            type: UPDATE_POPULAR_CARDS, 
            payload: { 
                cards: [{id: '1', title:'some title', images: {'480w_still': {}} }]
            }
        })).toEqual([{id: '1', title:'some title', thumb: {}}]);
      })

})
