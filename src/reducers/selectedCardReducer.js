import { UPDATE_SELECTED_CARD, CLEAR_SELECTED_CARD } from '../constants';
import { cardDataNormalize } from './_utilities';

const selectedCardReducer = (cards = [], action) => {
  if (action.type === UPDATE_SELECTED_CARD) {
    return cardDataNormalize(action.payload.card)
  }

  if (action.type === CLEAR_SELECTED_CARD) {
    return {}
  }

  return cards;
}

export default selectedCardReducer;