import { ERROR } from '../constants';

const errorReducer = (error = {}, action) => {
  if (action.type === ERROR) {
    return action.payload;
  }

  return error;
}

export default errorReducer;