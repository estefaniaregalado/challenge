import cardsReducer from './cardsReducer';
import { UPDATE_ALL_CARDS } from '../constants';

const initalState = [];

describe('test cards reducer', () => {
    it('update all items in the array', () => {
        expect(cardsReducer(initalState, { 
            type: UPDATE_ALL_CARDS, 
            payload: { 
                cards: [{id: '1', title:'some title', images: {'480w_still': {}} }]
            }
        })).toEqual([{id: '1', title:'some title', thumb: {}}]);
      })

})
