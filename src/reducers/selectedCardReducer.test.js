import { UPDATE_SELECTED_CARD, CLEAR_SELECTED_CARD } from '../constants';
import selectedCardReducer from './selectedCardReducer';

const initalState = {};

describe('test selectedCard reducer', () => {
    it('should set selected card data', () => {
        expect(selectedCardReducer(initalState, { 
            type: UPDATE_SELECTED_CARD,
            payload: { 
                card: {id: '1', title:'some title', images: {original: {}} }
            }
        })).toEqual({id: '1', title:'some title', image: {}});
    })

    it('should clear selected card data', () => {
        expect(selectedCardReducer({id: '1', title:'some title', image: {}}, { 
            type: CLEAR_SELECTED_CARD
        })).toEqual({});
    })

})
