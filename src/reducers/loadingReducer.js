import { LOADING } from '../constants';

const loadingReducer = (loading = [], action) => {
  if (action.type === LOADING) {
    return action.payload.loading;
  }

  return loading;
}

export default loadingReducer;