  
import { combineReducers } from 'redux';
import cards from './cardsReducer';
import popularCards from './popularCardsReducer';
import error from './errorReducer';
import loading from './loadingReducer';
import selectedCard from './selectedCardReducer';

export default combineReducers({cards, error, loading, popularCards, selectedCard});