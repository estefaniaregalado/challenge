import { all, call, put, takeEvery } from 'redux-saga/effects';
import { FETCH_CARDS, FETCH_POPULAR_CARDS, FETCH_SELECTED_CARD } from '../constants';
import { updateAllCards, updatePopularCards, updateSelectedCard } from '../actions/cardsActions';
import { setError } from '../actions/globalActions';
import { getCards, getPopularCards, getCardById } from '../helpers/api';

export default function* rootSaga() {
    yield all([fetchCards(), fetchPopularCards(), fetchSelectedCard()]);
}

export function* fetchCards() {
    yield takeEvery(FETCH_CARDS, makeCardsApiRequest);
}

export function* makeCardsApiRequest(action) {
    try {
        const req = yield call(getCards, {q: action.payload.query});
        yield put(updateAllCards(req));
    }
    catch(error) {
        yield put(setError({msg: 'Oops... Something went wrong'}))
    }
}

export function* fetchPopularCards() {
    yield takeEvery(FETCH_POPULAR_CARDS, makePopularCardsApiRequest);
}

export function* makePopularCardsApiRequest() {
    try {
        const req = yield call(getPopularCards, {});
        yield put(updatePopularCards(req));
    }
    catch(error) {
        yield put(setError({msg: 'Oops... Something went wrong'}))
    }
}

export function* fetchSelectedCard() {
    yield takeEvery(FETCH_SELECTED_CARD, makeSelectedCardApiRequest);
}

export function* makeSelectedCardApiRequest(action) {
    try {
        const req = yield call(getCardById, action.payload.id);
        yield put(updateSelectedCard(req));
    }
    catch(error) {
        yield put(setError({msg: 'Oops... Something went wrong'}))
    }
}
